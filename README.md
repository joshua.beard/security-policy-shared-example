# Example Security Policy Repo

This repository contains a GitLab security policy and a shared SAST ruleset.

The companion project that sources this repository is at
<https://gitlab.com/joshua.beard/example-project-using-security-policy>

## References

* <https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html>
* <https://www.youtube.com/watch?v=GsZEiXveDRM>
* <https://gitlab.com/gitlab-org/gitlab/-/issues/393452/>
